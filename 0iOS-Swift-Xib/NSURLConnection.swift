//  The converted code is limited to 1 KB.
//  Please Sign Up (Free!) to double this limit.
// 
//  Converted to Swift 5 by Swiftify v5.0.37171 - https://objectivec2swift.com/
class ViewController {

    private var responseData: Data?
    private var connection: NSURLConnection?
    private var array: [AnyHashable] = []

    func viewDidLoad() {
        super.viewDidLoad()

    }

    func connection(_ connection: NSURLConnection, didReceive response: URLResponse) {
        responseData.length = 0
    }

    func connection(_ connection: NSURLConnection, didReceive data: Data) {
        responseData.append(data)
    }

    func connection(_ connection: NSURLConnection, didFailWithError error: Error) {
        print("Connection failed: \(error.description())")
    }

    func connectionDidFinishLoading(_ connection: NSURLConnection) {

		let responseString = String(data: responseData, encoding: .utf8)
		responseData = nil

		var e: Error? = nil
		let jsonData = responseString?.data(using: .utf8)
		var JSON: [AnyHashable : Any]? = nil
		do {
			if let jsonData = jsonData {
				JSON = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [AnyHashable : Any]
			}
		} catch let e {
		}

		if JSON != nil {

			if let object = JSON?["title"] {
				array.append(object)
			}

			let alertView = UIAlertView(title: "iOSConnectPHPJson", message: array[2], delegate: nil, cancelButtonTitle: "close", otherButtonTitles: "")
			alertView?.show()
		}
	}
}